<?php

function readTripFile($filename) {
	$content = file_get_contents($filename);
	if ($content === false) return NULL;
	
	$trip = json_decode($content, true);
	if ($trip === NULL) return NULL;
	
	$data = array();
	//start info
	if (!buildExtremeData($trip, $data, true)) return NULL;
	//end info
	if (!buildExtremeData($trip, $data, false))return NULL;
	//start time
	if (!isset($trip['fromTime'])) return NULL;
	$data['start_time'] = intval($trip['fromTime']);
	
	//stops
	buildPlace($trip, $data, 'stops');
	//bookmarks
	buildPlace($trip, $data, 'bookmarks');
	
	return $data;
}

function buildExtremeData($trip, &$data, $start) {
	if ($start) {
		$tk = 'fromLocation';
		$dk = 'start';
	} else {
		$tk = 'toLocation';
		$dk = 'end';
	}
	if (!isset($trip[$tk])) return false;
	$ext = $trip[$tk];
	if (!isset( $ext['lat'],  $ext['lon'])) return false;
	
	$data["{$dk}_lat"] = $ext['lat'];
	$data["{$dk}_lng"] = $ext['lon'];
	
	return true;
}

function buildPlace($trip, &$data, $key) {
	if (!(isset($trip[$key]) && is_array($trip[$key])) ) 
		return;
	
	foreach ($trip[$key] as $place) {
		$data[$key][] = json_encode($place);
	}
}