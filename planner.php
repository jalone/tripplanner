<?php

function toIndex() {
	header("Location: index.html");
	exit();
}

function loadFile() {
	if ($_FILES['tripfile']['error'] != UPLOAD_ERR_OK)
		toIndex();
	include_once 'tripreader.php';
	return readTripFile($_FILES['tripfile']['tmp_name']);
}

function newSearch() {
	foreach (array('start_lat','start_lng','end_lat', 'end_lng') as $k) {
		$res[$k] = $_REQUEST[$k];
	}
	if (isset($_REQUEST['start_time']) && is_numeric($_REQUEST['start_time'])) {
		$res['start_time'] = intval($_REQUEST['start_time']);
	} else {
		$res['start_time'] = 'undefined';
	}
	return $res;
}

$initData = NULL;
if (isset($_FILES['tripfile'])) {
	$initData = loadFile();
} elseif (isset($_REQUEST['start_lat'], $_REQUEST['start_lng'],
		$_REQUEST['end_lat'], $_REQUEST['end_lng']))
{
	$initData = newSearch();
} 
if ($initData == NULL) {
	toIndex();
}

?><!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Trip Planner</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="Trip Planner">
		<meta name="author" content="Sapienza University of Rome">
		
		<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
		<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
		<link href="css/planner.css" rel="stylesheet">
		
		<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
		<!--<link rel="stylesheet" href="css/ui-lightness/jquery-ui-1.10.0.custom.min.css" type="text/css" />-->
    	<link rel="stylesheet" href="css/jquery.ui.timepicker.css?v=0.3.2" type="text/css" />
		
	</head>

	<body>

		<nav class="navbar navbar-default navbar-inverse navbar-fixed-top" role="navigation">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
				<span class="sr-only">Abilita navigazione</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">Trip Planner</a>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse navbar-ex1-collapse">
				<ul class="nav navbar-nav">
					<li class="active"><a href="index.html">Home</a></li>
					<li><a href="#about" data-toggle="modal" data-target="#div-about" >Chi siamo</a></li>
					<!--<li style="width: 500px;"><a href="#" style="margin: 10px 20px 0px 20px; padding:0px; cursor:default;"><div id="loadbar"></div></a><li>-->
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li><a href="#" class="navbar-link" data-toggle="modal" data-target="#load-path-descriptor" >Carica Percorso</a></li>
				</ul>
			</div><!-- /.navbar-collapse -->
		</nav>

		<div id="main-container" class="container"   style="display:none; position: relative;">
		  <div class="row">
			<div class="col-md-3">
				<div class="well well-sm sidebar-nav" style="min-height: 640px">
					
					<div class="list-group" id="list-trip" >
						<div class="list-group-item" id="dest-from"></div>
					</div>
				
					<div class="list-group" id="list-stops" >
						<div class="list-group-item nav-header text-info">Soste programmate<!--<br/><small class="text-muted">(Trascina per ordinare)</small>--></div>					
						<small class="text-muted list-group-item clearfix" id="spot-id1">Seleziona una sosta dopo averla trovata attraverso la funzione cerca sosta.</small>
					</div>
					
					<div class="list-group" id="list-trip" >
						<div class="list-group-item" id="dest-to"></div>
					</div>
					
				</div><!--/.well -->
			</div><!--/span-->
			<div class="col-md-9">
			<div class="panel panel-default">
			
				<!-- Search bar -->
  				<div class="panel-heading">
				
					<!-- SEARCH STEP 1 -->
					<span id="search1" class="dropdown">
						 
						<strong>1</strong> <span class="glyphicon glyphicon-chevron-right"></span> 
						<a role="button" data-toggle="dropdown" data-target="#" id="search1-btn" class="btn btn-default btn-primary btn-sm dropdown-toggle" style="width:100px;margin-right:20px;">
							<strong>Cerca Sosta</strong>  <span class="caret"></span>
						</a>
					
						<ul class="dropdown-menu" style="margin-top:15px;width:250px;" id="list-spots" role="menu" aria-labelledby="dLabel">
							
    						<li role="presentation" class="divider"></li>
							<li role="presentation"><span class="text-info" style="padding: 3px 20px;">Punti di interesse</span></li>
    						<li role="presentation" class="divider"></li>
							
							<li role="presentation" ><a href="#" id="spot-restaurant"><img src="images/spot-type/spot-restaurant.png"><strong>Ristoranti 		<span class="pull-right">&raquo;</span></strong></a></li>
							<li role="presentation" ><a href="#" id="spot-bar"><img src="images/spot-type/spot-bar.png"><strong>				<span>Bar</span><span class="pull-right">&raquo;</span></strong></a></li>
							<li role="presentation" ><a href="#" id="spot-services"><img src="images/spot-type/spot-services.png"><strong>	Aree di Servizio 	<span class="pull-right">&raquo;</span></strong></a></li>
							<li role="presentation" ><a href="#" id="spot-fuel"><img src="images/spot-type/spot-fuel.png"><strong>			Carburante 			<span class="pull-right">&raquo;</span></strong></a></li>
							<li role="presentation" ><a href="#" id="spot-tourism"><img src="images/spot-type/spot-tourism.png"><strong>		Mete turistiche 	<span class="pull-right">&raquo;</span></strong></a></li>
							<li role="presentation" ><a href="#" id="spot-hotel"><img src="images/spot-type/spot-hotel.png"><strong>			Hotel 				<span class="pull-right">&raquo;</span></strong></a></li>
							<li role="presentation" ><a href="#" id="spot-camping"><img src="images/spot-type/spot-camping.png"><strong>		Campeggi 			<span class="pull-right">&raquo;</span></strong></a></li>
						</ul>
					</span>
					
					<!-- SEARCH STEP 2 -->
					<span id="search2" class="dropdown" >
					
						<strong>2</strong> <span class="glyphicon glyphicon-chevron-right"></span> 
						<a role="button" data-toggle="dropdown" data-target="#" id="search2-btn" class="btn btn-default btn-sm dropdown-toggle disabled" style="width:100px;margin-right:20px;">
							<strong>Filtra</strong>  <span class="caret"></span>
						</a>
						
						<ul class="dropdown-menu" id="list-filter" style="margin-top:15px; width:250px;" role="menu" aria-labelledby="dLabel">
			
    						<li role="presentation" class="divider"></li>
							<li role="presentation"><span class="text-info" style="padding: 3px 20px;">Cerca Luoghi</span></li>
    						<li role="presentation" class="divider"></li>
							<li role="presentation"><a  href="#"  id="filter-back"><strong>&laquo; Indietro</strong></a></li>
    						<li role="presentation" class="divider"></li>
							
							<li role="presentation" class="search-filter">
								<div class="nav-header" >
									<label class="radio inline" for="filter-time">
										<input id="filter-time" name="filter-type" value="div-filter-time" type="radio" autocomplete="off" style="margin-left:0px;margin-right:5px;"/>					
										<strong>Cerca per Orario</strong>
									</label>
								</div>
								<div id="div-filter-time" class=" filter-div" style="display: none;">
									<div class="row">
										<label class="help-inline hidden" for="filt-time">Attorno alle ore</label>
										<div class="col-xs-9" style="padding:0px;">
											<input class="input-sm form-control" autocomplete="off" type="text" id="filt-time" placeholder="Attorno alle ore _:_" />
										</div>
										<div class="col-xs-1" style="padding:0px;">
											<button type="button" class="btn btn-default btn-success btn-sm btn-apply-filter" autocomplete="off">
												<i class="glyphicon glyphicon-share-alt"></i>
											</button>
										</div>
									</div>
									<div id="filter-time-err" class="filter-err text-danger" style="display: none;">
										Errore!
									</div>
									
								</div>
							</li>
							<li role="presentation" class="divider"></li>
			
							<li role="presentation" class="search-filter">
								<div class="nav-header" >
									<label class="radio inline" for="filter-dist">
										<input id="filter-dist" name="filter-type" value="div-filter-dist" type="radio" autocomplete="off" style="margin-left:0px;margin-right:5px;"/>					
										<strong>Cerca per Distanza</strong>
									</label>
								</div>
								<div id="div-filter-dist" class="filter-div" style="display: none;">
									<div class="clearfix">
										<label class="help-inline input-sm pull-left" style="width: 100px"  for="filt-dist">Dopo Km:</label>
										<input class="pull-right text-info" style="	width: 50px;
																			background-color:transparent;
																			border: 0px solid; height="1em" type="text" id="filt-dist" placeholder="0 Km" readonly/>
									</div>
									<div class="side-slider" id="slider-filter2"></div>
									<div class="text-center" style="margin-top:20px;">
										<button id="btn-apply-filter" type="submit" class="btn btn-default btn-sm btn-success btn-apply-filter"
												autocomplete="off">
											<strong>Mostra sulla Mappa <i class="glyphicon glyphicon-share-alt"></i></strong>
										</button>
									</div>
								</div>
							</li>
							<li role="presentation" class="divider"></li>
							
							<li role="presentation" class="search-filter">
								<div class="nav-header" >
									<label class="radio inline" for="filter-zone">
										<input id="filter-zone" name="filter-type" value="div-filter-zone" type="radio" autocomplete="off" style="margin-left:0px;margin-right:5px;"/>					
										<strong>Cerca per Zona</strong>
									</label>
								</div>
								<div id="div-filter-zone" class="filter-div" style="display: none;">
									<div class="row clearfix">
										<div class="col-xs-9" style="padding:0px;">
											<input type="text" autocomplete="off" class="form-control search-query input-sm" id="search-zone-string"/>
										</div>
										<div class="col-xs-1" style="padding:0px;">
											<button type="button" class="btn btn-default btn-success btn-sm btn-apply-filter" autocomplete="off">
												<i class="glyphicon glyphicon-share-alt"></i>
											</button>
										</div>
										
									</div>
								</div>
							</li>
						</ul>
					</span>

					<span class="pull-right"style="margin-top:5px;" id="info-trip-duration"></span>
						
				</div>
				
				<!-- The Map -->
  				<div class="panel-body">
					<div class="clearfix">
						
						<div id="planner-map" class="map-canvas google-map-canvas"  style="height: 570px;"></div>	
						
						<nav class="navbar" style="margin-top: 20px; position: absolute; bottom: 20px; right: 30px; width: 80%;" role="navigation">
							<form class="navbar-form navbar-right">
								<div class="btn-group">	
									<button type="button" id="btn-clean" 	class="btn btn-default">Pulisci</button>
									<button type="button" id="btn-directions" 	class="btn btn-default" data-toggle="modal" data-target="#div-directions" >Indicazioni</button>
									<button type="button" id="btn-bookmarks" 	class="btn btn-default" data-toggle="modal" data-target="#div-bookmarks" >Alternative</button>
									<button type="button" id="btn-export" 		class="btn btn-default">Esporta</button>
									<button type="button" id="btn-print" 		class="btn btn-default" style="display: none;"><i class="glyphicon glyphicon-print"></i></button>
									<button type="button" id="btn-save" 		class="btn btn-primary">Salva</button>
								</div>
							</form>
							<form class="hidden" action="download.php" method="post" id="save-form">
								<input 	type="hidden" id="downloadContent" 	name="content" value="ciao">
								<input 	type="hidden" id="downloadFilename" name="filename" value="my-travel.trip">
								<input 	type="hidden" id="downloadType" 	name="type" value="trip">
							</form>
						</nav>
						
					</div>
					
				</div>
			</div>
			</div><!--/span-->
		  </div><!--/row-->
		</div><!--/.fluid-container-->		
		
		<!-- setup waitinig modal  -->
		<div id="setup-modal" class="modal fade" tabindex="-1" role="dialog"  data-backdrop="static" data-keyboard="false" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-body">
						Caricamento in corso...
						<div class="progress progress-striped active">
						  <div class="progress-bar"  role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
						  </div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<!-- directions popup -->
		<div id="div-directions" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="directionsLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
						<h4 class="modal-title" id="directionsLabel">Indicazioni Stradali</h4>
					</div>
					<div class="modal-body">
						<div id="directionsPanel"></div>
					</div>
					<div class="modal-footer">
						<button class="btn btn-default btn-primary" data-dismiss="modal" aria-hidden="true">Chiudi</button>
					</div>
				</div>
			</div>
		</div>

		<!-- bookmarks popup -->
		<div id="div-bookmarks" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="bookmarksLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
						<h4 class="modal-title" id="bookmarksLabel">Alternative Salvate <span class="glyphicon glyphicon-star"></span></h4>
					</div>
					<div class="modal-body">
						<div id="bookmarksPanel">
							<ul class="nav nav-list" id="list-bookmarks"  >
								<li id="bookmark-placeholder" class="clearfix">Nessuna alternativa salvato</li>
							</ul>
						</div>
					</div>
					<div class="modal-footer">
						<button class="btn btn-default btn-primary" data-dismiss="modal" aria-hidden="true">Chiudi</button>
					</div>
				</div>
			</div>
		</div>

		<!-- edit start popup -->
		<div id="div-edit-start" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="editStartLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
						<h4 class="modal-title" id="editStartLabel">Modifica Partenza</h4>
					</div>
					<div class="modal-body">
						<div id="editStartPanel">
							<div class="form-inline clearfix">
                                <input tabindex=1 id="fromTime"	class="form-control input-sm col-md-2" style="width:100px;" type="text" placeholder="Ora Partenza" pattern="((2[0-3]|[01]?[0-9]):[0-5]?[0-9])?" />
                                <button tabindex=3 id="start-btn" onClick="return false;" class="btn btn-default btn-sm col-md-1  pull-right"><i class="glyphicon glyphicon-search"></i></button>
                                <input tabindex=2 id="fromLocation" class="form-control input-sm col-md-9  pull-right" style="width:360px;" type="text" placeholder="Il tuo luogo di partenza" />
                            </div>
						</div>
					</div>
					<div class="modal-footer">
                    	<div class="btn-group">
							<button class="btn btn-default" data-dismiss="modal" aria-hidden="true">Annulla</button>
							<button id="doEditStart" class="btn btn-default btn-primary" data-dismiss="modal" aria-hidden="true">Modifica</button>
                        </div>
					</div>
				</div>
			</div>
		</div>

		<!-- edit arrival popup -->
		<div id="div-edit-arrival" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="editArrivalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
						<h4 class="modal-title" id="editArrivalLabel">Modifica Arrivo</h4>
					</div>
					<div class="modal-body">
						<div id="editArrivalPanel">
							<input tabindex=5 id="toLocation" class="form-control input-sm col-md-11" style="width:300px;" type="text" placeholder="Il tuo luogo di arrivo"/>
							<button tabindex=6 id="arrival-btn" onClick="return false;" class="btn btn-default btn-sm col-md-1"><span class="glyphicon glyphicon-search"></span></button>
						</div>
					</div>
					<div class="modal-footer">
                    	<div class="btn-group">
							<button class="btn btn-default" data-dismiss="modal" aria-hidden="true">Annulla</button>
							<button id="doEditArrival" class="btn btn-default btn-primary" data-dismiss="modal" aria-hidden="true">Modifica</button>
                        </div>
					</div>
				</div>
			</div>
		</div>

		<!-- about popup -->
		<div id="div-about" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="aboutLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
						<h4 class="modal-title" id="aboutLabel">Chi siamo</h4>
					</div>
					<div class="modal-body">
						<div id="aboutPanel">
							<p>TripPlanner è stata sviluppata dagli studenti dell'Università di Roma Sapienza nell'ambito di un progetto sull'interazione uomo-macchina.<br/></p>
							<p>Docente di riferimento:<br/>
								<a href="http://w3.di.uniroma1.it/it/docenti/Panizzi">Prof. Emanuele Panizzi</a> - Professore aggregato<br/>
							</p>
							<p>Responsabili sviluppo:<br/>
								<a href="http://www.jalone.net">Lorenzo Tognalini</a> - Laurea Magistrale in Intelligenza Artificiale e Robotica<br/>
								<a href="#">Flavio De Benedictis</a> - Laurea Magistrale in Informatica<br/>
								<a href="#">Oltion Doda</a> - Laurea Magistrale in Informatica
							</p>
							<p>Responsabili progettazione:<br/>
								<a href="#">Valentina Negro Maggio</a> - Laurea Magistrale in Intelligenza Artificiale e Robotica<br/>
								<a href="#">Ejona Mile</a> - Laurea Magistrale in Informatica
							</p>
						</div>
					</div>
					<div class="modal-footer">
						<button class="btn btn-default btn-primary" data-dismiss="modal" aria-hidden="true">Close</button>
					</div>
				</div>
			</div>
		</div>
		
		
				
		<!-- load path popup -->
		<div id="load-path-descriptor" class="modal fade">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h3 id="load-path-title">Carica Percorso</h3>
					</div>
					<div id="load-path-body" class="modal-body">
					Se hai gi&agrave; utilizzato i nostri servizi ed hai salvato il risultato puoi qui ricaricarlo e visualizzare e/o modificare la pianificazione di tale viaggio.</div>
					<div class="modal-footer">
					
						<div class="form-inline clearfix">
							<form action="planner.php" method="post" enctype="multipart/form-data">
								<button type="button" style="width:100px;" class="btn btn-default input-sm pull-left" data-dismiss="modal" aria-hidden="true">Annulla</button>
								<input type="submit"  class="btn btn-default btn-primary btn-sm pull-right" aria-hidden="true" value="Carica" />
								<input type="file" required="required" name="tripfile" class="pull-right" id="pathToLoad" />
							</form>
						</div>
						
					</div>
				</div>
			</div>
		</div>
				
				
		<!-- stop length amount  -->
		<div id="stop-length-descriptor" class="modal fade">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h3 id="stop-length-title">Durata della sosta</h3>
					</div>
					<div id="stop-length-body" class="modal-body form-inline">
						<label for="txtStopDuration">Quanto vuoi sostare in questo posto?</label> 
						<input type="text" style="width:80px;"  class="pull-right form-control col-md-2 input-sm" id="txtStopDuration" placeholder="minuti"/></div>
					<div class="modal-footer">
					
						<div class="form-inline clearfix">
							<button type="button" style="width:100px;" class="btn btn-default input-sm pull-left" data-dismiss="modal" aria-hidden="true">Annulla</button>
							<button type="button"  id="confirmAddStop" class="btn btn-default btn-success btn-sm pull-right" data-dismiss="modal" aria-hidden="true">Conferma</button>
						</div>
						
					</div>
				</div>
			</div>
		</div>
		
		
		<script type="text/javascript" src="http://code.jquery.com/jquery.js"></script>
		<script type="text/javascript" src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>

		<script type="text/javascript" src="js/jquery.blockUI.js"></script>
   		<script type="text/javascript" src="js/jquery.ui.timepicker.js?v=0.3.2"></script>
		<script type="text/javascript" src="js/ui-1.10.0/jquery.ui.core.min.js"></script>
		<script type="text/javascript" src="js/ui-1.10.0/jquery.ui.widget.min.js"></script>
		<script type="text/javascript" src="js/ui-1.10.0/jquery.ui.tabs.min.js"></script>
		<script type="text/javascript" src="js/ui-1.10.0/jquery.ui.position.min.js"></script>
	
		<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
	
		<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?libraries=places&sensor=false&key=AIzaSyCCVS1iCxUqXNNQP6Yyzz8J8cZ1k0zbbUA"></script>
		<script type="text/javascript" src="js/RouteBoxer_packed.js"></script>
	
		<script type="text/javascript" src="js/json2.js"></script>
		<script type="text/javascript" src="js/XMLWriter.js"></script>
		<script type="text/javascript" src="js/markerclusterer.js"></script>
	
		<script type="text/javascript" src="js/common-functions.js"></script>
		<script type="text/javascript" src="js/multirequest.js"></script>
		<script type="text/javascript" src="js/place.js"></script>
		<script type="text/javascript" src="js/request-google.js"></script>
		<script type="text/javascript" src="js/request-osm.js"></script>
		<script type="text/javascript" src="js/request-db.js"></script>
		<script type="text/javascript" src="js/planner-state.js"></script>
		<script type="text/javascript" src="js/planner-save.js"></script>
		<script type="text/javascript" src="js/planner.js"></script>

		<!-- <?php print_r($initData) ?> -->
		
<script type="text/javascript">
showLoading();
$('#main-container').show();

$(document).ready(function () {
	state.pauseListeners();
<?php  
	echo "initialize($initData[start_lat], $initData[start_lng], $initData[end_lat], $initData[end_lng], $initData[start_time]);\n";
	if (isset($initData['stops'])) {
		echo "\nvar st_place;\n";
		foreach ($initData['stops'] as $stop) {
			echo "st_place = loadPlace($stop);\n";
			echo "st_place.setLevelStop();\n";
			echo "stops.push(st_place);\n";
		}
		//echo "showRoute(false);\n";
	}
	if (isset($initData['bookmarks'])) {
		echo "\nvar bm_place;\n";
		foreach ($initData['bookmarks'] as $bm) {
			echo "bm_place = loadPlace($bm);\n";
			echo "bm_place.setLevelBookmark();\n";
			echo "addBookmark(bm_place);\n";
		}
	}
?>
	state.wakeUpListeners();
});
</script>
	</body>
</html>
