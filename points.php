<?php
/**
 * Interfaccia per l'estrazione di punti di interesse (POI) da OpenStreetMap
 * 
 * INPUT GET
 *   type     il tipo di POI da estrarre. Un valore tra i seguenti:
 *                 restaurant    ristoranti
 *                 bar           bar
 *                 services      aree di servizio
 *                 fuel          benzinai
 *                 tourism       mete turistiche
 *                 hotel         hotel
 *                 camping       campeggi
 *   bound    tipo di margine da considerare. Valori: circle, rects
 *   lat      [bound=circle] la latitudine del centro del cerchio
 *   lon      [bound=circle] la longitudine del centro del cerchio
 *   radius   [bound=circle] il raggio del cerchio in metri
 *   
 * INPUT POST (solo con bound=rects)
 *   rect     stringa JSON rappresentante un array di rettangoli. 
 *            Ogni rettangolo è un oggetto con i seguenti attributi:
 *                 left     la longitudine del lato sinistro (più a ovest) del rettangolo 
 *                 bottom   la latitudine del lato inferiore (più a sud) del rettangolo
 *                 right    la longitudine del lato destro (più a est) del rettangolo
 *                 top      la latitudine del lato superiore (più a nord) del rettangolo 
 *   
 * OUTPUT
 *   un array JSON contenente un oggetto per ogni POI con le seguenti proprietà:
 *       lat    latitudine del POI
 *       lon    longitudine del POI
 *       id     ID del POI in OpenStreetMap
 *       nome   nome italiano del POI, eventualmente vuoto
 * 
 * 
 * ESEMPIO circle
 *   points.php?type=restaurant&bound=circle&lat=41.89154387998115&lon=12.491626739501953&radius=500
 * 
 * ESEMPIO rects
 *   points.php?type=restaurant&bound=rects
 *     POST rect=[{"top":41.931904648177564,"left":12.41709382925842,"bottom":41.85096570364489,"right":12.453344414629214},
 *                {"top":41.95888429635513,"left":12.453344414629214,"bottom":41.85096570364489,"right":12.562096170741597}]
 * 
 * 
 * TODO
 *   abbinare i tipi a più tag OSM (es. restaurant a [amenity=restaurant] e [amenity=fast_food])
 * 
 * 
 * TIPI => TAG OSM
 *   restaurant    [amenity=restaurant] { [amenity=fast_food] }
 *   bar           [amenity=bar] { [amenity=cafe] [amenity=pub] }
 *   services      [highway=services]
 *   fuel          [amenity=fuel]
 *   tourism       [tourism=attraction] { [historic=archaeological_site] [tourism=museum] }
 *   hotel         [tourism=hotel] { [tourism=hostel] [tourism=motel] [=] }
 *   camping       [tourism=camp_site] { [tourism=caravan_site] }
 */

if (isset($_GET['type'])) {
	switch($_GET['type']) {
		case 'restaurant':
			$key='amenity';
			$val='restaurant';
			break;
		case 'bar':
			$key='amenity';
			$val='bar';
			break;
		case 'services':
			$key='highway';
			$val='services';
			break;
		case 'fuel':
			$key='amenity';
			$val='fuel';
			break;
		case 'tourism':
			$key='tourism';
			$val='attraction';
			break;
		case 'hotel':
			$key='tourism';
			$val='hotel';
			break;
		case 'camping':
			$key='tourism';
			$val='camp_site';
			break;
		default:
			exit();
	}
}

if (isset($_GET['bound']) && $_GET['bound'] == 'circle') {
	//ricerca per zona
	if (!(isset($_GET['lat']) && isset($_GET['lon']) && isset($_GET['radius'])))
		exit();
	
	$data = "[out:json];node[\"$key\"=\"$val\"](around:";
	$data .= "$_GET[radius],$_GET[lat],$_GET[lon]);out body;";
} else {
	//ricerca per rettangoli
	if (!isset($_POST['rect'])) exit();
	$rect = json_decode($_POST['rect']);
	if ($rect === NULL || !is_array($rect)) exit();
	
	$data = '[out:json];(';
	//(node[\"$key\"=\"$val\"]($bottom,$left,$top,$right););out body;")
	foreach($rect as $r) {
		$data .= "node[\"$key\"=\"$val\"]($r->bottom,$r->left,$r->top,$r->right);";
	}
	$data .= ');out body;';
}
if (isset($_GET['debug']) && $_GET['debug']=='data') exit($data);

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, 'http://overpass-api.de/api/interpreter');
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_HEADER, false);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

$res = curl_exec($ch);
curl_close($ch);

if (isset($_GET['debug']) && $_GET['debug']=='res') exit($res);

$elList = array();
$obres = json_decode($res);
foreach($obres->elements as $o) {
	$el = array(
		'id'=>$o->id,
		'lat'=>$o->lat,
		'lon'=>$o->lon,
		'nome'=>''
	);
	if (property_exists($o, 'tags')) { 
		$tags = get_object_vars($o->tags);
		if (isset($tags['name']))
			$el['nome'] = $tags['name'];
		if (isset($tags['www.prezzibenzina.it'])) {
			$el['prezzibenzina'] = $tags['www.prezzibenzina.it'];
		}
	}
	$elList[] = $el;
}

echo json_encode($elList);

