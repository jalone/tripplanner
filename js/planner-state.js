
//***********************//
//** TRIP STATE        **//
//***********************//

/// hold the selected markers and relative infos in a sparse associative map where key are markers id

var state = {
		start: null,
		end: null,
		startTime: null,
		
		totalDistance: 0, // kilometers
		totalDuration: 0, // minutes

		//variabili private
		_listeners: {},
		_paused: false,
		_pauseEvents: {}
};

state.init = function(startLat, startLng, endLat, endLng, startTime) {
	var list = state._listeners;
	state._listeners = {};
	
	state.setStartPosition(startLat, startLng);
	state.setEndPosition(endLat, endLng);
	state.setStartTime(startTime);
	
	state._listeners = list;
}

state.setStartPosition = function(lat, lng) {
	reverseGeocodeCoord(lat, lng, function(loc) {
		if (state.start != null)
			state.start.updateMarker(null);
		state.start = new Place(new ExtremesPlaceLoader({
			lat: lat,
			lng: lng,
			name: loc[0].formatted_address,
		}), 'start');
		
		state._changed('start');
		state._changed('extremes');
	});
}

state.setStartString = function(str) {
	var geocoder = new google.maps.Geocoder();
	geocoder.geocode({'address': str, language: 'it'}, function(results, status){
		if (status == google.maps.GeocoderStatus.OK) {
			state.setStartPosition(results[0].geometry.location.lat(), results[0].geometry.location.lng());
		}
	});
}

state.setEndPosition = function(lat, lng) {
	reverseGeocodeCoord(lat, lng, function(loc) {
		if (state.end != null)
			state.end.updateMarker(null);
		state.end = new Place(new ExtremesPlaceLoader({
			lat: lat,
			lng: lng,
			name: loc[0].formatted_address,
		}), 'end');
		
		state._changed('end');
		state._changed('extremes');
	});
}

state.setEndString = function(str) {
	var geocoder = new google.maps.Geocoder();
	geocoder.geocode({'address': str, language: 'it'}, function(results, status){
		if (status == google.maps.GeocoderStatus.OK) {
			state.setEndPosition(results[0].geometry.location.lat(), results[0].geometry.location.lng());
		}
	});
}

state.setStartTime = function(min) {
	if (min == undefined) {
		var now = new Date();
		state.startTime = now.getHours()*60 + now.getMinutes();
	} else {
		state.startTime = min;
	}
	state._changed('startTime');
}

state.setStartTimeString = function(hhmm) {
	if (hhmm == undefined) {
		var now = new Date();
		state.startTime = now.getHours()*60 + now.getMinutes();
	} else {
		state.startTime = hhmmToMinutes(hhmm);
	}
	state._changed('startTime');
}

state.setTotalValues = function(dist, duration) {
	state.totalDistance = dist;
	state.totalDuration = duration;
	state._changed('totalDistance');
	state._changed('totalDuration');
	state._changed('totalValues');
}

state.getArrivalTime = function() {
	return (state.startTime + state.totalDuration)  % 1440;
}

state.isReady = function() {
	return state.start != null && state.end != null && state.startTime != null;
}

state.onchange = function(variab, func) {
	if (state._listeners.hasOwnProperty(variab)) {
		state._listeners[variab].push(func);
	} else {
		state._listeners[variab] = [func];
	}
}

state.pauseListeners = function() {
	if (!state._paused) {
		state._paused = true;
		state._pauseEvents = {};
	}
}

state.wakeUpListeners = function() {
	state._paused = false;
	for(ev in state._pauseEvents) {
		state._changed(ev);
	}
}

state._changed = function(variab) {
	if (state._pause) {
		state._pauseEvents[variab] = variab;
	} else {
		if (state._listeners.hasOwnProperty(variab)) {
			var lv = state._listeners[variab];
			for (var i=0; i<lv.length; i++) {
				lv[i].call(this, variab);
			}
		}
	}
}

/**
 * ExtremesPlaceLoader
 * load and inject data in the place object
 **/ 

function ExtremesPlaceLoader(info) {
	this._info = info;
}

ExtremesPlaceLoader.prototype.init = function(place){
	if (this._info == undefined) return;
	place.id 	= null;
	place.lat 	= this._info.lat;
	place.lon 	= this._info.lng;
	place._name = this._info.name;
	place._level = 2;
	place.createMarker(map);
}

ExtremesPlaceLoader.prototype.loadLazy = function(place, callback) {
	place._description = this.formatDescription();
	callback();
}

ExtremesPlaceLoader.prototype.formatDescription = function() {
	return '';
};

ExtremesPlaceLoader.prototype.getSaveAttributes = function() {
	return null;
}

ExtremesPlaceLoader.prototype.loadSavedAttributes = function(attr) {}


var bookmarks 	= new Object(); 

function addBookmark(place) {
	bookmarks[place.id] = place;
	state._changed('bookmarks');
}

function removeBookmark(id) {
	if (bookmarks.hasOwnProperty(id)) {
		delete bookmarks[id];
		state._changed('bookmarks');
		return true;
	}
	return false;
}

/** 
 *  hold the selected markers and relative infos in a sparse associative map where: 
 *  key are markers id, and value the 'marker' itself and the 'duration' of the stop and 'order' of the stop (first stop has 0 index)
 *  
**/

var stops = [];

stops.clear = function() {
	while(this.length > 0)
		this.pop();
}

stops.reorder = function(order){
	var tmp = [];
	for(var i=0; i<order.length; i++) {
		tmp.push(stops[order[i]]);
	}
	for(var i=0; i<order.length; i++) {
		stops[i] = tmp[i];
	}
	state._changed('stops');
}

stops.getStop = function(id){
	for(var i = 0; i< this.length; i++){
		if(this[i].id == id){
			return this[i];
		}
	}
	return null;
} 

stops.remove = function(id){
	for(var i = 0; i< this.length; i++){
		if(this[i].id == id){
			for(var j = i+1; j < this.length; j++) {
				this[j-1] = this[j]; 
			}
			this.pop();
			state._changed('stops');
			return true;
		}
	}
	return false;
}

stops.exists = function(id){
	for(var i = 0; i< this.length; i++){
		if(this[i].id == id){
			return true;
		}
	}
	return false;
}




/** this is to store querystring parameter once
 *	urlParams['fromLocation']; //possibly as lat,lng
 *	urlParams['toLocation']; //possibly as lat,lng
 *	urlParams['fromTime']; //in minutes
 *	urlParams['toTime']; //in minutes (DEPRECATED)
**/
//TODO eliminare
var urlParams;


//**************************//
//** TRIP CALCULATED VARS **//
//**************************//

//TODO elimiare
// travel informations
var totalDistance = 0; // kilometers
var totalDuration = 0; // minutes


//**************************//
//** PARAMETERS            **//
//**************************//

var spots = {
	"restaurant" : "Ristoranti",
	"bar" : "Bar",
	"services" : "Aree di Servizio",
	"fuel" : "benzinai",
	"tourism" : "Mete Turistiche", 
	"hotel" : "Hotel",
	"camping" : "Campeggi" 
};

var ZONE_FILTER_RADIUS  = 20000; //20Km
var DIST_FILTER_RADIUS  = 10; //10Km
var TIME_FILTER_THS 	= 15; //15 minutes


//***********************//
//** APPLICATION STATE **//
//***********************//

/// array that hold the places result from the last load
var placesResult = {};
function cleanPlaces(){ //questo e' un mio membro >:(
	for(var key in placesResult){	
		placesResult[key]._marker.setMap(null);
		delete placesResult[key];
	}
}


var selectedMarker = null; //a Place

// hold the the last selected spot type if any
var selectedSpotType;
var selectedSpotName;

//hold the selected filter
var filterName = 'none';
var searchArea;

// map variables
var directionsDisplay;
var alternativeDisplay;
var directionsService = new google.maps.DirectionsService();
var map;
var markerArray = [];
var directions; //hold the result of the google directions api request
var infowindow = new google.maps.InfoWindow({content: ""});
var startMarker = null;
var endMarker = null;

// route boxer
var rboxer = new RouteBoxer();
var BOXER_DISTANCE = 3; // km
var clusterArray = [];
var markerClusterer;

//oggetto PlacesRequest attuale
var request = null;

var firstRoute = true;