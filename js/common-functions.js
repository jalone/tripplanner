//***********************//
//** COMMON FUNCTIONS  **//
//***********************//

function countObjectMembers(obj){
    var c= 0;
    for(var p in obj){
		if(obj.hasOwnProperty(p)){
			++c;
		}
	}
    return c;
}

///convert a screen pixel unit relative to the map div in  lat long point on the map
function fromPixelToLatLng(pixel) {
	var scale = Math.pow(2, Map.getZoom());
	var proj = Map.getProjection();
	var bounds = Map.getBounds();
	
	var nw = proj.fromLatLngToPoint(
	new google.maps.LatLng(
		bounds.getNorthEast().lat(),
		bounds.getSouthWest().lng()
	));
	var point = new google.maps.Point();
	
	point.x = pixel.x / scale + nw.x;
	point.y = pixel.y / scale + nw.y;
	
	return proj.fromPointToLatLng(point);
}

///convert a lat long point on the map in screen pixel unit relative to the map div
function fromLatLngToPixel(position) {
	var scale = Math.pow(2, map.getZoom());
	var proj = map.getProjection();
	var bounds = map.getBounds();
	
	var nw = proj.fromLatLngToPoint(
	new google.maps.LatLng(
		bounds.getNorthEast().lat(),
		bounds.getSouthWest().lng()
	));
	var point = proj.fromLatLngToPoint(position);
	
	return new google.maps.Point(
	Math.floor((point.x - nw.x) * scale),
	Math.floor((point.y - nw.y) * scale));
}

function isValidTime(val) {
	return /^\s*(2[0-3]|[01]?[0-9])\s*:\s*[0-5]?[0-9]\s*$/.test(val);
}

/// convert minutes number into a readable string lik HH:mm
function minutesToHHmm(mins){
	var hours = Math.floor(mins / 60);
	var minutes = mins - (hours * 60);
	
	if(hours < 10){ hours = '0' + hours;}
	
	if(minutes < 10){ minutes = '0' + minutes;}
	
	return hours+':'+minutes;
}

function getTimeComponents(str) {
	var hm = str.split(':');
	hm[0] = parseInt(hm[0],10);
	hm[1] = parseInt(hm[1],10);
	return hm;
}

function hhmmToMinutes(str){
	var minutes = 0;
	var hm = str.split(':');
	minutes = (parseInt(hm[0],10) *60) + parseInt(hm[1],10);
	return minutes;
}

///check wether a key exists in an array
function arrayHasOwnIndex(array, prop) {
    return array.hasOwnProperty(prop) && /^0$|^[1-9]\d*$/.test(prop) && prop <= 4294967294; // 2^32 - 2
}

function reverseGeocode(value, callback) {
	var match = /\(\s*(\d+(?:\.\d+)?)\s*,\s*(\d+(?:\.\d+)?)\s*\)/.exec(value);
	if (match == null) return callback([{formatted_address: value}]);
	reverseGeocodeCoord(match[1], match[2], callback);
}

function reverseGeocodeCoord(lat, lng, callback) {
	var coord = new google.maps.LatLng(lat, lng);
	new google.maps.Geocoder().geocode({'latLng': coord}, callback);
}

function geoDistance(pt1, pt2) {
	var R = 6371; // km
	var dLat = (pt2.lat()-pt1.lat()) * Math.PI / 180;
	var dLon = (pt2.lng()-pt1.lng()) * Math.PI / 180;
	var lat1 = pt1.lat() * Math.PI / 180;
	var lat2 = pt2.lat() * Math.PI / 180;

	var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
		Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2); 
	var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
	return R * c;
}
