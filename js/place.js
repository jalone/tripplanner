function Place(loader, type){
	this._loader = loader;
	
	this.type = type;
	
	this.id = '';
	this.lat = 0;
	this.lon = 0;
	
	this._name = null;
	this._description = null;
	
	this._level = 0; //0 result, 1 bookmark, 2 stop
	this.mins 	= 0;

	this._marker = null;

	loader.init(this);
	
	this._route = null;
}

Place.prototype.getName = function(){
	if(this._name == null){
		console.log("Error: don't call 'getName' if 'load' has not returned")
	}
	return this._name;
}

Place.prototype.createMarker = function(map){
	this._marker = new google.maps.Marker({
						position: new google.maps.LatLng(this.lat, this.lon),
						map: map
						});
	this._updateIcon();
	return this._marker;
}

/// return a google latlng object
Place.prototype.getPosition = function(){
	return this._marker.getPosition();
}

Place.prototype.getSaveObject = function(){
	var obj = { lat: this.lat, 
				lon: this.lon,
				id: this.id, 
				type: this.type, 
				mins: this.mins,
				_name: this._name,
				_loaderInfo: this._loader.getSaveAttributes()
			};
	return obj;
}

Place.prototype.loadData = function(obj) {
	this.lat = obj.lat;
	this.lon = obj.lon;
	this.id = obj.id;
	this.type = obj.type;
	this.mins = obj.mins;
	this._name = obj._name;
	
	this._loader.loadSavedAttributes(obj._loaderInfo);
}


Place.prototype.loadDescription = function(callback) {
	if (this._description == null) {
		var origThis = this;
		this._loader.loadLazy(this, function() {
			callback(origThis, origThis._name, origThis._description);
		});	
	} else {
		callback(this, this._name, this._description);
	}
}

Place.prototype.setMarker = function(marker){
	this._marker = marker; 
	this._updateIcon();
}

Place.prototype.updateMarker = function(map) {
	this._marker.setMap(map);
}

Place.prototype.removeFromClusterer = function(clusterer) {
	if (clusterer !== undefined) {
		var map = this._marker.getMap();
		clusterer.removeMarker(this._marker);
		this._marker.setMap(map);
	}
}

Place.prototype._updateIcon = function(){
	if (this._marker == null) return;
	var strlv;
	switch(this._level){
		case 0:
			strlv = 'marker';
			break;
		case 1:
			strlv='bookmark';
			break;
		case 2:
			strlv='stop';
			break;
	}
	this._marker.setIcon('images/markers/'+strlv+'-'+this.type+'.png');
}

Place.prototype.isStop = function (){
	return this._level == 2;
}

Place.prototype.isBookmark = function (){
	return this._level == 1;
}
	
Place.prototype.setLevelStop = function (mins){
	this._level = 2; //0 result, 1 bookmark, 2 stop
	if (mins != undefined)
		this.mins = mins;
	this._updateIcon();
}
	
Place.prototype.setLevelBookmark = function (){
	this._level = 1; //0 result, 1 bookmark, 2 stop
	this.mins 	= 0;
	this._updateIcon();
}
	
Place.prototype.resetLevel = function (){
	this._level = 0; //0 result, 1 bookmark, 2 stop
	this.mins 	= 0;
	this._updateIcon();
}
	
Place.prototype.setRoute = function(route) {
	this._route = route;
}

Place.prototype.unsetRoute = function() {
	this._route = null;
}

Place.prototype.getRoute = function() {
	return this._route;
}