//VERSIONE OPENSTREETMAP

/**
 * OSMPlaceLoader
 * load and inject data in the place object
 **/ 

function OSMPlaceLoader(info) {
	this._info = info;
}

OSMPlaceLoader.prototype.init = function(place){
	if (this._info == undefined) return;
	place.id 	= this._info.id;
	place.lat 	= this._info.lat;
	place.lon 	= this._info.lon;
	place._name = this._info.nome;
}

OSMPlaceLoader.prototype.loadLazy = function(place, callback) {
	place._description = this.formatDescription();
	callback();
}

OSMPlaceLoader.prototype.formatDescription = function() {
	if (this._info.hasOwnProperty('prezzibenzina')) {
		return '<a href="http://www.prezzibenzina.it/distributori/'+this._info.prezzibenzina+'#pano" target="_blank">Prezzi</a>'
			+ '<br><a href="http://www.prezzibenzina.it/distributori/'+this._info.prezzibenzina+'" target="_blank">Dettagli</a>';
	}
	return '';
};

OSMPlaceLoader.prototype.getSaveAttributes = function() {
	if (this._info.hasOwnProperty('prezzibenzina'))
		return { prezzibenzina: this._info.prezzibenzina };
	else
		return {};
}

OSMPlaceLoader.prototype.loadSavedAttributes = function(attr) {
	this._info = {};
	if (attr.hasOwnProperty('prezzibenzina'))
		this._info.prezzibenzina = attr._prezzibenzina;
}


/**
 * OSMPlacesRequest
 * load and create the places object
 **/ 

function OSMPlacesRequest(map, type, callback) {
	this.type = type;
	this.callback = callback;
	this.active = true;
}

//disabilita la chiamata alla callback a fine richiesta
OSMPlacesRequest.prototype.disable = function() {
	this.active = false;
};

OSMPlacesRequest.prototype.requestDone = function(arg) {
	if (this.active) {
		var res = [];
		for(var i=0; i<arg.length; i++)
			res.push(new Place(new OSMPlaceLoader(arg[i]), this.type));
		this.callback.apply(this, [res]);
	}
};

//effettua la richiesta usando i rettangoli come delimitatori
// rects: array di oggetti con prop { top, left, bottom, right }
OSMPlacesRequest.prototype.getRects = function(rects) {
	var req = this;
	$.post('points.php?type='+this.type, {rect: JSON.stringify(rects)}, 
		function(arg) {req.requestDone(arg);}, 
		'json')
	.fail(function( jqxhr, textStatus, error ) {
		var err = textStatus + ', ' + error;
		console.log( "Request Failed: " + err);
		//TODO show error: "failed to download data from server"
	});
};

//effettua la richiesta usando un delimitatore circolare
OSMPlacesRequest.prototype.getCircle = function(lat, lng, radius) {
	var req = this;
	$.getJSON('points.php', {
			type: this.type,
			bound: 'circle',
			lat: lat,
			lon: lng,
			radius: radius
		})
		.done(function(arg) {req.requestDone(arg);})
		.fail(function( jqxhr, textStatus, error ) {
			var err = textStatus + ', ' + error;
			console.log( "Request Failed: " + err);
			//TODO show error: "failed to download data from server"
			onSpotsUpdated( new Object()  );
		});
};
