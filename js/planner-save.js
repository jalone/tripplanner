
//*********************************//
//** SAVING AND EXPORT FUNCTIONS **//
//*********************************//

function addPlaceNode(XML, type, place, num){
	XML.BeginNode(type);
		XML.Attrib('lon', place.lon+'');
		XML.Attrib('lat', place.lat+'');
		if(num != 0){XML.Node('number', num+'');}
		//TODO add time
		XML.Node('ele',' 0.0');
		XML.Node('name', place.getName());
		XML.Node('type', place.type+'');
	XML.EndNode();
}

$('#btn-export').click(function(e){
	
	var XML = new XMLWriter();
	//try{ 
		//header
		XML.BeginNode("gpx");
			XML.Attrib("xmlns", "http://www.topografix.com/GPX/1/1");
			XML.Attrib("version", "1.1");
			XML.Attrib('creator', 'TripPlanner')
			
			//header
			XML.BeginNode("metadata");
				XML.Node('name', 'TripPlanner trip plan');
				XML.Node('desc', 'from '+state.start.getName()+' to '+state.end.getName());
				//XMl.Node('copyright','?');
				XML.BeginNode('link');
					XML.Attrib('href','http://www.triplanner.com');
					XML.Node('text', 'TripPlanner');
				XML.EndNode();
				XML.Node('time','<TODO TIMESTAMP EG2009-10-17T22:58:43Z>');
				XML.Node('keywords','<TODO>');
				XML.BeginNode('bounds');
					XML.Attrib('minlat','<TODO>59.4367664166667'); //TODO
					XML.Attrib('maxlat','<TODO>59.4440920666666');//TODO
					XML.Attrib('minlon','<TODO>24.74394385');//TODO
					XML.Attrib('maxlon','<TODO>24.7971432');//TODO
				XML.EndNode();
			XML.EndNode();
			
			//route
			XML.BeginNode('rte');
				XML.Node('name','Percorso');
				
				var num = 0;
				
				//start
				XML.BeginNode('rtept');
					XML.Attrib('lon', state.start.lon+'');//TODO
					XML.Attrib('lat', state.start.lat+'');//TODO
					//TODO add time
					XML.Node('', '0');
					XML.Node('number', num+'');
					XML.Node('ele',' 0.0');
					XML.Node('name', state.start.getName());
					XML.Node('type', 'start');
				XML.EndNode();
				
				//stops
				for (var index = 0; index < stops.length; ++index) {
					addPlaceNode(XML, 'rtept', stops[index], num++);
				}
				//arrival							
				XML.BeginNode('rtept');
					XML.Attrib('lon', state.end.lon+'');//TODO
					XML.Attrib('lat', state.end.lat+'');//TODO
					//TODO add time
					XML.Node('number', num++ +'');
					XML.Node('ele',' 0.0');
					XML.Node('name', state.end.getName());
					XML.Node('type', 'arrival');
				XML.EndNode();
				
			XML.EndNode();//end routes
			
			//alternatives
			for(var index in bookmarks){
				addPlaceNode(XML, 'wpt', bookmarks[index], 0);
			}
			
		XML.EndNode();
		XML.Close(); // Takes care of unended tags.
	
	//					XML.WriteString("Blah blah.");
		
	//}catch(Err){
		//alert("Error: " + Err.description);
	//}
	
	var gpx = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>' + XML.ToString().replace(/</g,"\n<");

	$('#downloadContent').val(gpx);
	$('#downloadFilename').val("my-travel.gpx");
	$('#downloadType').val("gpx");
	
	$('#save-form').submit();
	
	return true;
});

$('#btn-save').click(function(e){
	
	var jsondata = JSON.stringify(buildSaveObject());

	$('#downloadContent').val(jsondata);
	$('#downloadFilename').val("my-travel.trip");
	$('#downloadType').val("trip");
	
	$('#save-form').submit();
	
	return true;
});

$('#btn-print').click(function(e){
	//TODO put here print script
	return false;
});

function buildSaveObject() {
	var objs = {};
	
	//save route
	objs["fromLocation"] 	= state.start.getSaveObject(); 		//urlParams['fromLocation'];
	objs["toLocation"]	 	= state.end.getSaveObject(); 			//urlParams['toLocation'];
	objs["fromTime"] 		= state.startTime; 			//urlParams['fromTime'];
	objs["toTime"]			= state.getArrivalTime(); 	//urlParams['toTime'];

	//save stops
	objs["stops"]		= [];
	for (var index = 0; index < stops.length; ++index) {
		objs["stops"].push(stops[index].getSaveObject());
	}
	
	//save bookmarks
	objs["bookmarks"] 	= [];
	for(var index in bookmarks){
		objs["bookmarks"].push(bookmarks[index].getSaveObject()); 
	}
	return objs;
}

function loadSavedObject(objs) {
	urlParams["fromLocation"] 	= objs['fromLocation'];
	urlParams["toLocation"]	 	= objs['toLocation'];
	urlParams["fromTime"] 		= objs['fromTime'];
	urlParams["toTime"]			= objs['toTime'];

	cleanPlaces();
	createMap();

	stops.clear();
	for (var index = 0; index < objs["stops"].length; ++index) {
		var place = loadPlace(objs["stops"][index]);
		if (place != undefined) {
			place.setLevelStop(place.mins);
			stops.push(place);
		}
	}
	refreshStopsDiv();
		
	bookmarks = {};
	for (var index = 0; index < objs["bookmarks"].length; ++index) {
		var place = loadPlace(objs["bookmarks"][index]);
		if (place != undefined) {
			place.setLevelBookmark();
			bookmarks[place.id] = place;
		}
	}
	refreshBookmarksDiv();
	
	showRoute(false);

}