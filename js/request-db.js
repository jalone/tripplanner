//VERSIONE DATABASE LOCALE

/**
 * DBPlaceLoader
 * load and inject data in the place object
 **/ 

function DBPlaceLoader(info) {
	this._info = info;
}

DBPlaceLoader.prototype.init = function(place){
	if (this._info == undefined) return;
	place.id 	= this._info.id;
	place.lat 	= this._info.lat;
	place.lon 	= this._info.lon;
	place._name = this._info.nome;
}

DBPlaceLoader.prototype.loadLazy = function(place, callback) {
	place._description = this.formatDescription();
	callback();
}

DBPlaceLoader.prototype.formatDescription = function() {
	if (this._info.prezzibenzina != null) {
		return '<a href="http://www.prezzibenzina.it/distributori/'+this._info.prezzibenzina+'#pano" target="_blank">Prezzi</a>'
			+ '<br><a href="http://www.prezzibenzina.it/distributori/'+this._info.prezzibenzina+'" target="_blank">Dettagli</a>';
	}
	return '';
};

DBPlaceLoader.prototype.getSaveAttributes = function() {
	return {
		prezzibenzina: this._info.prezzibenzina
	};
}

DBPlaceLoader.prototype.loadSavedAttributes = function(attr) {
	this._info = {
			prezzibenzina: attr.prezzibenzina
	};
}


/**
 * DBPlacesRequest
 * load and create the places object
 **/ 

function DBPlacesRequest(map, type, callback) {
	this.type = type;
	this.callback = callback;
	this.active = true;
}

//disabilita la chiamata alla callback a fine richiesta
DBPlacesRequest.prototype.disable = function() {
	this.active = false;
};

DBPlacesRequest.prototype.requestDone = function(arg) {
	if (this.active) {
		var res = [];
		for(var i=0; i<arg.length; i++)
			res.push(new Place(new DBPlaceLoader(arg[i]), this.type));
		this.callback.apply(this, [res]);
	}
};

//effettua la richiesta usando i rettangoli come delimitatori
// rects: array di oggetti con prop { top, left, bottom, right }
DBPlacesRequest.prototype.getRects = function(rects) {
	var req = this;
	$.post('dbpoi.php', {rect: JSON.stringify(rects)}, 
		function(arg) {req.requestDone(arg);}, 
		'json')
	.fail(function( jqxhr, textStatus, error ) {
		var err = textStatus + ', ' + error;
		console.log( "Request Failed: " + err);
		//TODO show error: "failed to download data from server"
	});
};

//effettua la richiesta usando un delimitatore circolare
DBPlacesRequest.prototype.getCircle = function(lat, lng, radius) {
	var area = new google.maps.Circle({center: new google.maps.LatLng(lat,lng), radius: radius}).getBounds();
	var rect = {
			top:	area.getNorthEast().lat(),
			left:	area.getSouthWest().lng(),
			bottom:	area.getSouthWest().lat(),
			right:	area.getNorthEast().lng()
		};
	this.getRects([rect]);
};
