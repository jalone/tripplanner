//*******************//
//** MAP FUNCTIONS **//
//*******************//

function initialize(startLat, startLng, endLat, endLng, startTime) {
	addElemEvents();
	createMap();
	alternativeDisplay = new google.maps.DirectionsRenderer({
		suppressMarkers : true, 
		preserveViewport: true,
		polylineOptions: {
			strokeColor: 'green',
			strokeOpacity: 0.5,
			strokeWeight: 5,
			zIndex: -1
		}
	});
	google.maps.event.addListener(infowindow,'closeclick',function(){
		   alternativeDisplay.setMap(null);
		});
	
	state.init(startLat, startLng, endLat, endLng, startTime);
}

function createMap() {
	gplaces = null;
	var mapOptions = {
			zoom: 5,
			center: new google.maps.LatLng(41.901255,12.465134),
			mapTypeId: google.maps.MapTypeId.ROADMAP
	}
	
	directionsDisplay = new google.maps.DirectionsRenderer({suppressMarkers : true, preserveViewport: false});

	map = new google.maps.Map(document.getElementById("planner-map"), mapOptions);
	directionsDisplay.setMap(map);
	directionsDisplay.setPanel(document.getElementById("directionsPanel"));
}

function showRoute(preserveViewport) {
	if (preserveViewport == undefined)
		preserveViewport = false;
	
	//unsets all cached routes
	for(var i in bookmarks) {
		bookmarks[i].unsetRoute();
	}
	for(var i=0; i<stops.length; i++) {
		stops[i].unsetRoute();
	}
	for(var i in placesResult) {
		placesResult[i].unsetRoute();
	}
	
	directionsDisplay.setOptions({suppressMarkers : true, preserveViewport: preserveViewport});
	computeRoute(null, function(result, status) {
								if (status == google.maps.DirectionsStatus.OK) {
									directionsDisplay.setDirections(result);
									directions = result;
									
									//reorders stops according to the order used by google
									stops.reorder(result.routes[0].waypoint_order);
									
									//compute statistics about directions
									var routeinfo = getRouteInfo(directions.routes[0]);
									
									state.setTotalValues(routeinfo.distance, routeinfo.time);
									if (firstRoute) {
										//first time we show a route
										firstRoute = false;
										hideLoading();
										// Activate search menu right-away	
										$('#list-spots').slideToggle('slow');

									}
								}
							}
							);
}

function computeRoute(extrawp, callback) {
	if (!state.isReady())
		return;
	
	var wypt = [];
	for (index = 0; index < stops.length; ++index) {
		wypt.push({location: stops[index].getPosition(), stopover:true});
	}
	if (extrawp != null) wypt.push({location: extrawp.getPosition(), stopover:true});
	
	var request = {
		origin:			state.start.getPosition(),
		waypoints: 		wypt,
		destination: 	state.end.getPosition(),
		optimizeWaypoints: true,
		travelMode: 	google.maps.TravelMode.DRIVING
	};
	
	directionsService.route(request, callback);
}

function OnHourShowCallback(hour) {
	if ((hour > Math.floor(state.getArrivalTime()/60)) || (hour < Math.floor(state.startTime/60))) { 
		return false; // not valid
	}
	return true; // valid
}

function OnMinuteShowCallback(hour, minute) {
	var startMin = state.startTime/60;
	var arrivalMin = state.getArrivalTime()/60;
	if ((hour == Math.floor(arrivalMin)) && (minute >= ((arrivalMin)%1)*60) ) { return false; } // not valid
	if ((hour == Math.floor(startMin))	&& (minute < ((startMin)%1)*60) ) { return false; }   // not valid
	return true;  // valid
}

function getRouteInfo(route) {
	var legs = route.legs;
	var res = {distance: 0, time: 0};
	
	for(var i=0; i<legs.length; ++i) {
		res.distance += legs[i].distance.value;
		res.time += legs[i].duration.value;
	}
	//add stop duration
	for (index = 0; index < stops.length; ++index) {
		res.time += stops[index].mins * 60;
	}
	
	res.distance = res.distance/ 1000;
	res.time = Math.round( res.time / 60 );
	
	return res;
}

function getStopInfo(route, index) {
	var legs = route.legs;
	var res = {distance: 0, time: 0};
	
	if (index < 0 || index >= legs.length)
		index = legs.length-1;
	
	for(var i = 0; i <= index; ++i) {
		res.distance += legs[i].distance.value;
		res.time += legs[i].duration.value;
	}
	//add stop duration
	for (var i = 0; i < index; ++i) {
		res.time += stops[i].mins * 60;
	}
	
	res.distance = res.distance/ 1000;
	res.time = Math.round( res.time / 60 );
	
	return res;
}

function purgeMap(){
	cleanPlaces();
	if (markerClusterer != undefined) {
		markerClusterer.clearMarkers();
		clusterArray = [];
	}
}

function zoomToShowAll(){
	
	var bounds =  directions.routes[0].bounds;
	for(var i = 0; i < placesResult.length; i++){
		bounds.extend(placesResult[i].getPosition());
	}
	for(var index in bookmarks){
		bounds.extend(bookmarks[index].getPosition());
	}
	for(var i = 0; i<stops.length; i++){
		bounds.extend(stops[i].getPosition());
	}
	map.fitBounds(bounds);
	
}

/// convert the data retrieved from server to map markers and show them
function showMarkers(places) {
	purgeMap();
	
	//fill with the new ones
	for (var i = 0; i < places.length; i++) {
		var id = places[i].id;
		//dont show marker if there already is a bookmark or a stop
		if (!(bookmarks.hasOwnProperty(id) || stops.exists(id))){
			var marker = places[i].createMarker(map);
			placesResult[id] = places[i];
			attachInstructionText(marker, places[i]);
			clusterArray.push(marker);
		}
	}
	
	var mcOptions = {gridSize: 30, maxZoom: 15};
	markerClusterer = new MarkerClusterer(map, clusterArray, mcOptions);
	
}

function showAlternativeRoute(route) {
	alternativeDisplay.setDirections(route);
	alternativeDisplay.setMap(map);
}

function attachInstructionText(marker, place) {

	google.maps.event.addListener(marker, 'click', function() {
			
		var content = '<div>Attendi...</div>';
		infowindow.setContent(content);
		
		var req = new MultiRequest(1, fillInfoWindow);
		var data = {name: '', description: ''};
		
		if (!place.isStop()) {
			if (place.getRoute() == null) {
				var idr = req.addRequest();
				//compute route
				computeRoute(place, function(result, status) {
					if (status == google.maps.DirectionsStatus.OK) {
						place.setRoute(result);
						showAlternativeRoute(result);
						req.requestDone(idr, [place, data]);
					}
				});
			} else {
				showAlternativeRoute(place.getRoute());
			}
		}
		place.loadDescription(function(place, name, description) {
			data.name = name;
			data.description = description;
			req.requestDone(0, [place, data]);
		});
		infowindow.open(map, marker);
		
		selectedMarker = place;
	});
}

function fillInfoWindow(place, data) {
	var name = data.name;
	var content = 
		'<div>'+
			'<p><strong class="uppercase text-primary">' +
				(name == "" ? "SENZA NOME" : name) + '</strong>' +
				//'<a href="#" class="text-muted pull-right" style="text-decoration:none;" id="add-bookmark"><strong>&#9734;</strong></a>'+
			'</p>' +
			'</p>' +
				data.description+ //'This is a short description of the Place.'+
			'</p>';
	
	//show route differences
	if (!place.isStop()) {
		var waypoint_order = place.getRoute().routes[0].waypoint_order;
		var legs = place.getRoute(0).routes[0].legs;
		currentpoint_position = waypoint_order.indexOf(waypoint_order.length - 1);
		var time = 0;
		var distance = 0;
		for (var i = 0; i <= currentpoint_position; i++	) {
			time += legs[i].duration.value;
			distance += legs[i].distance.value;
			
		}
		//add stop duration
		for (index = 0; index < currentpoint_position; ++index) {
			time += stops[waypoint_order[index]].mins * 60;
		}
		time = Math.round( time / 60 );
		distance = distance / 1000;
		
		content += '<p>Arrivo previsto per le: ' + minutesToHHmm(time + state.startTime);
		content += '<br />' + Math.round(distance) + ' Km dalla partenza</p>';
		
		var info = getRouteInfo(place.getRoute().routes[0]);
		var dl = Math.round((info.distance - state.totalDistance)*10)/10;
		var dt = Math.round(info.time - state.totalDuration);
		if (dl>0) dl = '+'+dl;
		if (dt>0) dt = '+'+dt;
		content += '<p>Differenza: '+dl+' km, '+dt+' min</p>';
		
	}
	
	content +=	
			//is just a place
			(!(place.isBookmark() || place.isStop()) ? 
			'<div style="margin-top: 5px;">' +
				'<label style="margin-right: 10px;">Aggiungi come: </label>' +
				'<span class="btn-group form-inline">' +
					'<button type="button" 	class="btn btn-warning btn-sm" id="add-bookmark">Alternativa</button>'+
					'<button type="button" 	class="btn btn-success btn-sm" id="add-stop">Sosta</button>'+
					//'<input  type="text" 	class="col-md-3 btn btn-sm" id="add-stop-minutes" placeholder="minuti" style="border-color: #46c;"/>'+
				'</span>'+
			'</div>' : 
			
			//is a bookmark
			( place.isBookmark() ?
			'<div style="margin-top: 5px;">' +
				'<span class="btn-group form-inline">' +
					'<button type="button" 	class="btn btn-danger btn-sm" id="rm-bookmark">Rimuovi</button>' +
					'<button type="button" class="btn btn-success btn-sm" id="add-stop">Aggiungi Sosta</button>'+
				'</span>'+
			'</div>' : 
			
			//is a stop
			'<div style="margin-top: 5px;">'+
				'<label style="margin-right: 10px;">Sosta ('+place.mins+' minuti) </label>' +
				'<span class="btn-group form-inline">'+
					'<button type="button" 	class="btn btn-danger btn-sm" id="rm-stop">Rimuovi</button>'+
				'</span>'+
			'</div>')) +
		
		'</div>';
	infowindow.setContent(content);

	google.maps.event.addListener(infowindow, 'domready', function(){ 
	
		//call back for the Bookmark button, add the currently selected marker to the bookmarks list
		$('#add-bookmark').click(function(e){
			if (bookmarks.hasOwnProperty(selectedMarker.id)){ 
				return false;
			}
			movePlace(selectedMarker, 1);
			infowindow.close();
			alternativeDisplay.setMap(null);
			return false;
		});
		
		//call back for the Add Stop button, add the currently selected marker to the bookmarks list
		$('#add-stop').click(function(e){
			if(stops.length >= 6){
				alert("Non e' possibile effettuare piu' di 6 soste.");
				return false;
			}
			$('#stop-length-descriptor').modal('show');
			return false;
		});
		$('#confirmAddStop').click(function(e){
			if(stops.exists(selectedMarker.id)){
				return false;
			}
			var minutes = parseInt($('#txtStopDuration').val(),10);
			if(!minutes){
				minutes = 0;
			}
			selectedMarker.justAdded = true;
			movePlace(selectedMarker, 2, minutes);
			infowindow.close();
			alternativeDisplay.setMap(null);
			
			$('#stop-length-descriptor').hide(500).delay(500).modal('hide');
			return true;
		});
		
		//call back for the Add Stop button, add the currently selected marker to the bookmarks list
		$('#rm-stop').click(function(e){
			if(!stops.exists(selectedMarker.id)){
				return false;
			}
			movePlace(selectedMarker, 0);
			infowindow.close();
			alternativeDisplay.setMap(null);
			return false;
		});
		
		//call back for the Add Stop button, add the currently selected marker to the bookmarks list
		$('#rm-bookmark').click(function(e){
			if(!bookmarks.hasOwnProperty(selectedMarker.id)){
				return false;
			}
			movePlace(selectedMarker, 0);
			infowindow.close();
			alternativeDisplay.setMap(null);
			return false;
		});
 	});	
}

function movePlace(place, level, stopMins){
	
	var doReinitialize = false;

	if(place.isBookmark()){
		if(level == 1){return;}
		removeBookmark(place.id);
	}else if(place.isStop()){
		if(level == 2){return;}
		stops.remove(place.id);
		doReinitialize = true;
	}else{
		if(level == 0){return;}
		delete placesResult[place.id];
		place.removeFromClusterer(markerClusterer);
	}
	
	switch(level){
		case 0: //to just a result
			placesResult[place.id] = place;
			place.resetLevel();
			break;
		case 1: //to bookmark
			addBookmark(place);
			place.setLevelBookmark();
			break;
		case 2: //to stop
			stops.push(place);
			place.setLevelStop(stopMins);
			place.unsetRoute();
			doReinitialize = true;
			break;
	}
					
	if(doReinitialize){
		showRoute(true);
	}
}


//******************************//
//** SPOT SELECTION FUNCTIONS **//
//******************************//



function getPlaceProvider(type) {
	switch(type) {
	case "services" : 
		return 'db';
	case "fuel" :
	case "tourism" :
		return 'osm';
	default:
		return 'google';
}
}
 
function newPlacesRequest(map, type, callback) {
	var provider = getPlaceProvider(type);
	switch(provider) {
		case 'osm' :
			return new OSMPlacesRequest(map, type, callback);
		case 'google':
			return new GooglePlacesRequest(map, type, callback);
		case 'db':
			return new DBPlacesRequest(map, type, callback);
	}
}

function loadPlace(obj) {
	var provider = getPlaceProvider(obj.type);
	var loader;
	switch(provider) {
		case 'osm' :
			loader = new OSMPlaceLoader();
			break;
		case 'google':
			loader = new GooglePlaceLoader();
			break;
		case 'db':
			loader = new DBPlaceLoader();
			break;
		default:
			return undefined;
	}
	var place = new Place(loader, obj.type);
	place.loadData(obj);
	var marker = place.createMarker(map);
	attachInstructionText(marker, place);
	return place;
}


//***********************************************//
//** BOOKAMRKS AND STOPS MANAGEMENTS FUNCTIONS **//
//***********************************************//

function refreshBookmarksDiv(){
	$('#list-bookmarks').empty();

	var numBookmarks = 0;
	
	for(var index in bookmarks) {
		numBookmarks++;
		$('#list-bookmarks').append('<li id="spot-'+ 
								bookmarks[index].id +
								'" class="list-group-item clearfix"><a href="#" class="pull-left" style="margin-right: 40px;" >'+
								'<img src="images/spot-type/bookmark-'+bookmarks[index].type+'.png">'+
								(bookmarks[index].getName() == "" ? "SENZA NOME" : bookmarks[index].getName() ) +
								'</a><button type="button" id="remove-spot-'+ 
								bookmarks[index].id+ 
								'" data-id="'+ 
								bookmarks[index].id+ 
								'" class="close pull-right spot-remover" aria-hidden="true">&times;</button></li>');
								
		$('#btn-bookmarks').delay(100).effect('highlight',{color:'#cccc22'},2000);
		
		$('#remove-spot-' + bookmarks[index].id).click(function(e){            
			var id = $(e.target).data('id');
			movePlace(bookmarks[id], 0);
			infowindow.close();
			alternativeDisplay.setMap(null);
			return false;
		});
	}
	
	if(numBookmarks == 0){
		$('#list-bookmarks').append('<li id="bookmark-placeholder" class="clearfix">Nessun alterativa salvata</li>');
	}
}

function refreshStopsDiv(){
	$('#list-stops').empty();
	$('#list-stops').append('<div class="list-group-item nav-header text-info">Soste programmate<br/></div>');

	var numStops = stops.length;
	
	
	if(stops.length == 0){
		$('#list-stops').append('<small class="text-muted list-group-item clearfix" id="spot-id1">Seleziona una sosta dopo averla trovata attraverso la funzione cerca sosta.</small>');
	}
	
	for(var i = 0; i<stops.length; i++){
		var info = getStopInfo(directions.routes[0], i);
		var div = $('<div id="spot-'+ 
								stops[i].id +
								'" class="list-group-item clearfix"><a href="#" class="pull-left text-info" style="margin-right: 40px;" >'+
								'<span class="ui-icon-arrowthick-2-n-s"></span><img src="images/spot-type/stop-'+stops[i].type+'.png">'+ 
								(stops[i].getName() == "" ? "SENZA NOME" : stops[i].getName() ) + 
								' (' + stops[i].mins +"') "+
								'</a><br><small class="pull-left">Arrivo pervisto per le '+
								minutesToHHmm(info.time + state.startTime)+
								'<br>'+
								Math.round(info.distance)+
								' Km dalla partenza</small><button type="button" id="remove-stop-'+ 
								stops[i].id+ 
								'" data-id="'+ 
								stops[i].id+ 
								'" class="close pull-right spot-remover" aria-hidden="true">&times;</button></div>');
								
		
		$('#list-stops').append(div);
		if(stops[i].hasOwnProperty('justAdded')) {
			delete stops[i].justAdded;
			div.delay(500).effect('highlight',{color:'#55dd55'},2000);
		} else {
			div.show();
		}
		
		$('#remove-stop-' + stops[i].id).click(function(e){
			
			var id = $(e.target).data('id');
			movePlace(stops.getStop(id), 0);
			return false;
		});
	}
}


//**********************//
//** FILTERS FUNCTION **//
//**********************//

function goToStep(step) {
	if (step < 1) {
		$('#search1-btn').addClass('disabled');
	} else {
		$('#search1-btn').removeClass('disabled');
	}
	
	if (step == 1) {
		$('#search1-btn').addClass('btn-primary').removeClass('btn-info');
		//$('#list-spots').slideToggle('slow');
	} else {
		if(step > 1) {
			$('#search1-btn').addClass('btn-info');
		} else {
			$('#search1-btn').removeClass('btn-info');
		}
		$('#search1-btn').removeClass('btn-primary');
	}
	
	if (step < 2) {
		$('#search2-btn').addClass('disabled').removeClass('btn-primary');
		$('#list-filter').slideUp('slow');
	} else {
		$('#search2-btn').removeClass('disabled').addClass('btn-primary');
		$('.filter-err').hide();
		$('#list-filter').slideDown('slow');
	}
}

//apply the filter
function applyFilter(e){
	//check for valid input
	var valid = true;
	switch (filterName) {
		case "time":
			var input = $('#filt-time').val();
			var err = null;
			if (! isValidTime(input)) {
				err = "Inserire un orario hh:mm";
			} else {
				var hm = getTimeComponents(input);
				if (!OnHourShowCallback(hm[0]) || !OnMinuteShowCallback(hm[0], hm[1])) {
					err = "Orario non compreso tra " + minutesToHHmm(state.startTime) 
							+ " e " + minutesToHHmm(state.getArrivalTime());
				}
			}
			if (err != null) {
				valid = false;
				$("#filter-time-err").text(err).show();
			} else {
				$("#filter-time-err").hide();
			}
			break;
	}
	if (!valid) return false;
	
	showLoading();

	goToStep(0);
	purgeMap();
	
	switch (filterName) {

		//// FILTER BY TIME APPLED
		case "time":
			var val = hhmmToMinutes($('#filt-time').val()) - state.startTime;
			askForSpots(extractSubroute(timeToDistance(val-TIME_FILTER_THS), timeToDistance(val+TIME_FILTER_THS)));
			break;
		
		//// FILTER BY DISTANCE APPLIED
		case "dist":
			var val =  parseInt($('#filt-dist').val(),10);
			console.log(val);
			askForSpots(extractSubroute(val-DIST_FILTER_RADIUS, val+DIST_FILTER_RADIUS));
			break;
		
		//// FILTER BY ZONE APPLIED
		case "zone":
			
			var place = $('#search-zone-string').val();
			var query = 'http://maps.googleapis.com/maps/api/geocode/json?sensor=false&language=it&address=' + place;
			
			var geocoder = new google.maps.Geocoder();
			
			geocoder.geocode( 	{ 'address': place, language: 'it'}, 
								function(results, status) {
									if (status == google.maps.GeocoderStatus.OK) {
									
										var point = results[0].geometry.location;										
										var radius 	= ZONE_FILTER_RADIUS; //20 km
										
										//zoom to the search area
										var area = new google.maps.Circle({center: point, radius: radius});
										if (!map.getBounds().contains(point))
											zoomToShowAll();

										request = newPlacesRequest(map,selectedSpotType,function(markers){
												//zoom to the search area
												fluidSetBounds(area.getBounds());
												onSpotsUpdated(markers);
											});
										request.getCircle(point.lat(), point.lng(), radius);
											
									} else {
										//alert("Indirizzo Sconosciuto: " + status);
										alert( "Nessun posto trovato con questo nome: '" + $('#search-zone-string').val() +"'");
										onSpotsUpdated( new Object()  );
									}
								});		
		
			break;
	}

	return false;
}

function onSpotsUpdated(markers){
	showMarkers(markers);
	$("#list-spots>li.active").removeClass("active");
	$('#spot-'+selectedSpotType).parent().addClass('active');
	goToStep(1);
	hideLoading();
};
    
function extractSubroute(start, end) {
    var route = directions.routes[0].overview_path;
    
    var distance = 0;
    
    var subroute = [];
    var iniziato = false;
    
    for (var i = 1; i < route.length; i++) {
		distance += geoDistance(route[i - 1], route[i]);
		if (iniziato) {
			subroute.push(route[i]);
			
			if (distance > end) {
				return subroute;
			}
		}
		else {
			if (distance > start) {
				subroute.push(route[i - 1]);
				subroute.push(route[i]);
				iniziato = true;
			}
		}
    }
    return subroute;
}

function askForSpots(route) {
    var boxes = rboxer.box(route, BOXER_DISTANCE);
			
    boxpolys = new Array(boxes.length);

    var bounds = null 
    var post = [];
    for (var i = 0; i < boxes.length; i++) {
    	if (bounds == null) {
    		bounds = new google.maps.LatLngBounds(boxes[i].getSouthWest(), boxes[i].getNorthEast());
    	} else {
    		bounds.union(boxes[i]);
    	}
    	var rect = {
				top:	boxes[i].getNorthEast().lat(),
				left:	boxes[i].getSouthWest().lng(),
				bottom:	boxes[i].getSouthWest().lat(),
				right:	boxes[i].getNorthEast().lng()
			};
		post.push(rect);
    }

    request = newPlacesRequest(map,selectedSpotType, function(markers){
    	//zoom to the search area
    	fluidSetBounds(bounds);
	    onSpotsUpdated(markers);
	});
    request.getRects(post);
    
    var mapb = map.getBounds();
    if (!map.getBounds().contains(bounds.getCenter())){
    	zoomToShowAll();
	}
}

function fluidSetBounds(bounds) {
	var az = map.getZoom();
	map.fitBounds(bounds);
	var zoom = map.getZoom();
	map.setZoom(az);
	
	if (Math.abs(az - zoom) < 2) {
		//direct zoom
		map.fitBounds(bounds);
	} else {
		map.panTo(bounds.getCenter());
		fluidZoomTo(zoom);
	}
}

function fluidZoomTo(zoom) {
	var az = map.getZoom();
	
	if (Math.abs(az - zoom) < 2) {
		map.setZoom(zoom);
	} else {
		if (az > zoom)
			map.setZoom(az - 2);
		else
			map.setZoom(az + 2);
		google.maps.event.addListenerOnce(map, 'idle', function() {fluidZoomTo(zoom);});
	}
}

function timeToDistance(time) {
	if(time < 0){
		return 0;
	}
    var durata = 0;
    var distance = 0;
    
    var durataStepCorrente, distanzaStepCorrente;
    
	for(var j = 0; j < directions.routes[0].legs.length; j++){
		for (var i = 0; i < directions.routes[0].legs[j].steps.length; i++) {
			durataStepCorrente = directions.routes[0].legs[j].steps[i].duration.value / 60;
			distanzaStepCorrente = directions.routes[0].legs[j].steps[i].distance.value / 1000;
		
			durata += durataStepCorrente;
		
			if (durata > time) {
				var t = time - (durata - durataStepCorrente);
				return distance + (distanzaStepCorrente / durataStepCorrente) * t;
			}
			distance += distanzaStepCorrente;
		}
		//add stop duration
		if(j<(directions.routes[0].legs.length-1)){
			durata += stops[j].mins;
			if (durata > time)
				return distance;
		}
	}
	
    return distance;
}

//**************************//
//** EDIT TRIP FUNCTIONS  **//
//**************************//

function addElemEvents() {
	//clean the map from unused markers
	$('#btn-clean').click(function (){
		purgeMap();
		showRoute(true);
		zoomToShowAll();
	});
	
	$("#search1-btn").click(function(){
		goToStep(1);
		$('#list-spots').slideToggle('slow');
		return true;
	});

	//after selecting a spot type
	$.each(spots, function(key, value) {
		$('#spot-' + key).click(function() {
			selectedSpotType = key;
			selectedSpotName = value;
			//$('#search2').show();
			goToStep(2);
			//$('#search1-btn').dropdown('toggle');
			$('#list-spots').slideToggle('slow');
			$('#filter-heading').text('Cerca per ' + value);
			return false;
		});
	});

	$('.search-filter').click(function(){
		//$('#search3').show();
		goToStep(3);
		return true;
	});
	
	//back button
	$('#filter-back').click(function() {
		$('#search1-btn').dropdown('toggle');
		goToStep(1);
		return false;
	});        

	//show/hide filtering sections
	$('input:radio').on('click',function(){
		$('#div-filter-time').hide();
		$('#div-filter-dist').hide();
		$('#div-filter-zone').hide();
		$('#'+$(this).val()).show();
		filterName = $(this).val().substr(11);
		return true;
	});



	// return trig filter
	$('#filt-time, #filt-dist, #search-zone-string').keypress(function(e) {
	    if(e.which == 13) {applyFilter();}
	});

	//focus select field content
	$("#filt-time").click(function() {
	  $("#filt-time").select();
	});
	$("#filt-dist").click(function() {
	  $("#filt-dist").select();
	});
	$("#search-zone-string").click(function() {
	  $("#search-zone-string").select();
	});

	$('.btn-apply-filter').click(applyFilter);
	
	$('#doEditStart').click(function(){
		state.setStartString($('#fromLocation').val());
		state.setStartTimeString($('#fromTime').val());
		return true;
	});
	
	$('#doEditArrival').click(function(){
		state.setEndString($('#toLocation').val());
		return true;
	});
	
	$('#fromTime').timepicker({
		showAnim: 'blind',
		hourText: 'Ore',              // Define the locale text for "Hours"
		minuteText: 'Minuti',         // Define the locale text for "Minute"
		amPmText: ['', ''],       	  // Define the locale text for periods
		hours: {
			starts: 0,	              //0 First displayed hour
			ends: 23                  //23 Last displayed hour
		},
		minutes: {
			starts: 0,                // First displayed minute
			ends: 55,                 // Last displayed minute
			interval: 5               // Interval of displayed minutes
		},
		rows: 4,                      // Number of rows for the input tables, minimum 2, makes more sense if you use multiple of 2
		
		// buttons
		showCloseButton: true,       // shows an OK button to confirm the edit
		closeButtonText: 'OK',       // Text for the confirmation button (ok button)
		showNowButton: true,         // Shows the 'now' button
		nowButtonText: 'Adesso', 	 // Text for the now button
		showDeselectButton: false,   // Shows the deselect time button
		deselectButtonText: 'Deselect' // Text for the deselect button
	});
	
	$('#filt-time').timepicker( {
		showAnim: 'blind',
		hourText: 'Ore',              // Define the locale text for "Hours"
		minuteText: 'Minuti',         // Define the locale text for "Minute"
		amPmText: ['', ''],       	  // Define the locale text for periods
		hours: {
			starts: 0,	              //0 First displayed hour
			ends: 23                  //23 Last displayed hour
		},
		minutes: {
			starts: 0,                // First displayed minute
			ends: 55,                 // Last displayed minute
			interval: 5               // Interval of displayed minutes
		},
		rows: 4,                      // Number of rows for the input tables, minimum 2, makes more sense if you use multiple of 2
		
		// buttons
		showCloseButton: true,       // shows an OK button to confirm the edit
		closeButtonText: 'OK',       // Text for the confirmation button (ok button)
		showNowButton: false,         // Shows the 'now' button
		nowButtonText: 'Adesso', 	 // Text for the now button
		showDeselectButton: false,   // Shows the deselect time button
		deselectButtonText: 'Deselect', // Text for the deselect button
		onHourShow: OnHourShowCallback,
		onMinuteShow: OnMinuteShowCallback
	} );
}

function updateStartDiv() {
	$('#dest-from').html('<a href="#" id="edit-from" class="dotdotdot">' + 
			state.start.getName() + 
		'</a></br>'+
		'<small>'+
			'Partenza alle ' + minutesToHHmm(state.startTime) +
			'<a href="#" class="pull-right" data-toggle="modal" data-target="#div-edit-start" id="editStart"><span class="glyphicon glyphicon-pencil"></span></a>'+ 
		'</small>');
}

function updateEndDiv() {
	$('#dest-to').html('<a href="#" id="edit-to" class="dotdotdot">' + 
			state.end.getName() + 
		'</a></br>'+ 
		'<small>'+
			'Arrivo previsto per le ' +  minutesToHHmm(state.getArrivalTime()) + 
			'<a href="#" class="pull-right" data-toggle="modal" data-target="#div-edit-arrival" id="editArrival"><span class="glyphicon glyphicon-pencil"></span></a>'+
		'</small>');
}

function showLoading(){
	$.blockUI({ message: 'Caricamento...<br/><div id="loadbar"></div>',
				css: { 
						border: 'none', 
						padding: '15px', 
						backgroundColor: '#000', 
						'-webkit-border-radius': '10px', 
						'-moz-border-radius': '10px', 
						opacity: .7, 
						color: '#fff' 
					} }); 
	$("#loadbar").progressbar({	value:false });
}

function hideLoading(){
	$("#loadbar").progressbar("destroy");
	$.unblockUI();
}

//***************************//
//** BEGIN PROCEDURAL PART **//
//***************************//

//$(document).ajaxStop(hideLoading()); 

state.onchange('start', updateStartDiv);
state.onchange('startTime', updateStartDiv);
state.onchange('end', updateEndDiv);
state.onchange('startTime', updateEndDiv);
state.onchange('totalValues',function() {
	$('#info-trip-duration').html('<strong>'+Math.round(state.totalDistance) + 
			'</strong><small> Km in </small><strong>' + 
			minutesToHHmm(state.totalDuration) + 
			'</strong><small> Hs</small>');
	updateEndDiv();
});
state.onchange('totalDistance', function() {
	$( "#slider-filter2" ).slider({
		range: false,
		min: 0,
		max: Math.round(state.totalDistance),
		values: Math.round(state.totalDistance/2),
		slide: function( event, ui ) {
			$( "#filt-dist" ).val(ui.value);
		}
	});
	$( "#filt-dist" ).val( $( "#slider-filter2" ).slider( "value"));
});

state.onchange('bookmarks',refreshBookmarksDiv);
state.onchange('startTime', refreshStopsDiv);
state.onchange('stops',refreshStopsDiv);
state.onchange('extremes', function() { showRoute(false); });
