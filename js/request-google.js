//VERSIONE GOOGLE PLACES LIBRARY

var gplaces = null;
function googlePlaces() {
	if (gplaces == null) gplaces = new google.maps.places.PlacesService(map);
	return gplaces;
}

/**
 * GooglePlaceLoader
 * load and inject data in the place object
 **/ 

function GooglePlaceLoader(placeResult) {
	this._placeResult = placeResult;
}

GooglePlaceLoader.prototype.init = function(place){
	if (this._placeResult == undefined) return;
	place.id 	= this._placeResult.id;
	place.lat 	= this._placeResult.geometry.location.lat();
	place.lon 	= this._placeResult.geometry.location.lng();
}

GooglePlaceLoader.prototype.loadLazy = function(place, callback) {
	var loader = this;
	googlePlaces().getDetails({reference: this._placeResult.reference}, function(result, status) {
		if (status == google.maps.places.PlacesServiceStatus.OK) {
			place._description = loader.formatDescription(result);
			place._name = result.name;
			callback();
		}
	});	
}

GooglePlaceLoader.prototype.formatDescription = function(result) {
	var info = '';
	if (result.rating != undefined)
		info += 'Valutazione: '+result.rating+' su 5<br>';
	info += '<a href="'+result.url+'" target="_blank">Dettagli</a>';
	if (result.website != undefined)
		info += '<br>Sito: <a href="'+result.website+'" target="_blank">'+result.website+'</a>';
	return info;
};

GooglePlaceLoader.prototype.getSaveAttributes = function() {
	return { reference: this._placeResult.reference };
}

GooglePlaceLoader.prototype.loadSavedAttributes = function(attr) {
	this._placeResult = {};
	this._placeResult.reference = attr.reference;
}




/**
 * GooglePlacesRequest
 * load and create the places object
 **/ 

function GooglePlacesRequest(map, type, callback) {
	this.type = type;
	this.callback = callback;
	this.active = true;
	
	switch(type) {
		case "restaurant" : 
			this.gtypes = ['restaurant'];
			break;
		case "bar" : 
			this.gtypes = ['bar','cafe'];
			break;
		case "services" : 
			this.gtypes = ['parking']; //TODO errato?
			break;
		case "fuel" : 
			this.gtypes = ['gas_station'];
			break;
		case "tourism" : 
			this.gtypes = ['museum','art_gallery'];
			break;
		case "hotel" : 
			this.gtypes = ['lodging'];
			break;
		case "camping" :
			this.gtypes = ['campground']; //TODO errato?
			break;
	}
}

//disabilita la chiamata alla callback a fine richiesta
GooglePlacesRequest.prototype.disable = function() {
	this.active = false;
};

GooglePlacesRequest.prototype.requestDone = function(reqid, results, status) {
	if (this.pending[reqid]) {
		this.pending[reqid] = false;
		this.totPending--;
	}
	if (this.active) {
		if (status == google.maps.places.PlacesServiceStatus.OK) {
			for (var i = 0; i < results.length; i++) {
				this.results.push(new Place(new GooglePlaceLoader(results[i]), this.type));
			}
		}
		if (this.totPending == 0)
			this.callback.apply(this, [this.results]);
	}
};

//effettua la richiesta usando i rettangoli come delimitatori
// rects: array di oggetti con prop { top, left, bottom, right }
GooglePlacesRequest.prototype.getRects = function(rects) {
	this.init(rects.length);
	for(var i=0; i<rects.length; i++) {
		this.doPlaceRequest(i, {
			bounds: new google.maps.LatLngBounds(
				new google.maps.LatLng(rects[i].bottom, rects[i].left),
				new google.maps.LatLng(rects[i].top, rects[i].right)
			)
		});
	}
};

//effettua la richiesta usando un delimitatore circolare
GooglePlacesRequest.prototype.getCircle = function(lat, lng, radius) {
	this.init(1);
	this.doPlaceRequest(0, {
		location: new google.maps.LatLng(lat, lng),
		radius: radius
	});
};

GooglePlacesRequest.prototype.doPlaceRequest = function(reqid, data) {
	data.types = this.gtypes;
	var req = this;
	googlePlaces().radarSearch(data, function(results, status) {req.requestDone(reqid, results, status);});
};

GooglePlacesRequest.prototype.init = function(num) {
	this.totPending = num;
	this.results = [];
	this.pending = [];	
	for(var i=0; i<num; i++)
		this.pending.push(true);
}