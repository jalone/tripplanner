function MultiRequest(num, finalCallback, scope) {
	this.callback = finalCallback;
	this.active = true;
	if (sope = null) 
		this.scope = this;
	else
		this.scope = scope;
	
	this.totPending = num;
	this.pending = [];	
	for(var i=0; i<num; i++)
		this.pending.push(true);
}

//aggiunge una nuova richiesta e restituisce l'indice
MultiRequest.prototype.addRequest = function() {
	this.pending.push(true);
	this.totPending = this.pending.length;
	return this.pending.length - 1;
}

//disabilita la chiamata alla callback a fine richiesta
MultiRequest.prototype.disable = function() {
	this.active = false;
};
 
MultiRequest.prototype.requestDone = function(reqid, args) {
	if (this.pending[reqid]) {
		this.pending[reqid] = false;
		this.totPending--;
	}
	if (this.active && this.totPending == 0) {
		this.callback.apply(this.scope, args);
	}
};