var geocoder;
var map = [];
var marker = [];

//time selector
$('#fromTime-str').timepicker( {
	showAnim: 'blind',
	hourText: 'Ore',              // Define the locale text for "Hours"
	minuteText: 'Minuti',         // Define the locale text for "Minute"
	amPmText: ['', ''],       	  // Define the locale text for periods
	hours: {
		starts: 0,	              //0 First displayed hour
		ends: 23                  //23 Last displayed hour
	},
	minutes: {
		starts: 0,                // First displayed minute
		ends: 55,                 // Last displayed minute
		interval: 5               // Interval of displayed minutes
	},
	rows: 4,                      // Number of rows for the input tables, minimum 2, makes more sense if you use multiple of 2
	
	// buttons
	showCloseButton: true,       // shows an OK button to confirm the edit
	closeButtonText: 'OK',       // Text for the confirmation button (ok button)
	showNowButton: true,         // Shows the 'now' button
	nowButtonText: 'Adesso', 	 // Text for the now button
	showDeselectButton: false,   // Shows the deselect time button
	deselectButtonText: 'Deselect' // Text for the deselect button
} );




function initialize() {
	geocoder = new google.maps.Geocoder();
	var myLatlng = new google.maps.LatLng(41.901255,12.465134);
	var mapOptions = {
		zoom: 5,
		center: myLatlng,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	}
	
	map[0] = new google.maps.Map(document.getElementById("start-map"), mapOptions);
	map[1] = new google.maps.Map(document.getElementById("arrival-map"), mapOptions);
	marker[0] = undefined;
	marker[1] = undefined;
	$('#start-btn, #arrival-btn').click(codeAddress);
	$('#fromLocation,#toLocation').keydown(function(e){
		if(e.keyCode == 13) e.preventDefault();
	}).keyup(function(e){
		if(e.keyCode == 13) {
			e.preventDefault();
			$(this).trigger("enterKey");
		}
	}).bind("enterKey",codeAddress).blur(codeAddress);
}

function codeAddress() {
	codeElemAddress($(this));
}

function codeElemAddress($th, callback) {
  var address = document.getElementById($th.data('addr')).value;
  var mapId = $th.data('mapid');
	
  geocoder.geocode( { 'address': address}, function(results, status) {
    if (status == google.maps.GeocoderStatus.OK) {
      map[mapId].setCenter(results[0].geometry.location);
      if (map[mapId].getZoom() < 11)
        map[mapId].setZoom(11);
      var icon;
      if (mapId == 0) icon = 'images/markers/marker-start.png';
      else icon = 'images/markers/marker-finish.png';
      if (marker[mapId] == undefined) {
	      marker[mapId] = new google.maps.Marker({
		  map: map[mapId],
		  position: results[0].geometry.location,
		  icon: icon,
		  draggable: true
	      });
      } else {
	      marker[mapId].setPosition(results[0].geometry.location);
      }
      if (callback != undefined) callback(true);
    } else {
      console.log('Geocode was not successful for the following reason: ' + status);
      if (callback != undefined) callback(false);
    }
  });
}

function submitForm() {
	if (marker[0] == undefined || marker[1] == undefined) return;
	
	var minutes = '';
	
	//set start_time
	var hmstr = $('#fromTime-str').val();
	if (hmstr != "") {
		var hmRegex = /^\s*((2[0-3]|[01]?[0-9])\s*:\s*[0-5]?[0-9])?\s*$/;
		if (hmRegex.test(hmstr)) {
			var hm = hmstr.split(':');
			minutes = (parseInt(hm[0],10)*60) + parseInt(hm[1],10);
		}
		//else time invalid
	}
	$('#start_time').val(minutes);
	
	//set start coord
	$('#start_lat').val(marker[0].getPosition().lat());
	$('#start_lng').val(marker[0].getPosition().lng());
	
	//set end coord
	$('#end_lat').val(marker[1].getPosition().lat());
	$('#end_lng').val(marker[1].getPosition().lng());
	
	$('#form_search').submit();
}

google.maps.event.addDomListener(window, 'load', initialize);
		  
$( document ).ready(function() {
	$('#start-btn, #fromLocation').data('addr','fromLocation').data('mapid',0);
	
	$('#arrival-btn, #toLocation').data('addr','toLocation').data('mapid',1);
	
	$('#submit').click(function() {
		var req = new MultiRequest(2, submitForm);
		
		if (marker[0] == undefined) {
			codeElemAddress($('#fromLocation'), function(success) {
				if (success) req.requestDone(0);
				else req.disable();
			});
		} else {
			req.requestDone(0);
		}
			
		if (marker[1] == undefined) {
			codeElemAddress($('#toLocation'), function(success) {
				if (success) req.requestDone(1);
				else req.disable();
			});
		} else {
			req.requestDone(1);
		}
	});
});