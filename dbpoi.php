<?php
/**
 * Interfaccia per l'estrazione di punti di interesse (POI) da database
 *   
 * INPUT POST (solo con bound=rects)
 *   rect     stringa JSON rappresentante un array di rettangoli. 
 *            Ogni rettangolo è un oggetto con i seguenti attributi:
 *                 left     la longitudine del lato sinistro (più a ovest) del rettangolo 
 *                 bottom   la latitudine del lato inferiore (più a sud) del rettangolo
 *                 right    la longitudine del lato destro (più a est) del rettangolo
 *                 top      la latitudine del lato superiore (più a nord) del rettangolo 
 *   
 * OUTPUT
 *   un array JSON contenente un oggetto per ogni POI con le seguenti proprietà:
 *       lat             latitudine del POI
 *       lon             longitudine del POI
 *       id              ID del POI nel database
 *       nome            nome del POI, eventualmente vuoto
 *       prezzibenzina   ID del benzinaio su Prezzi Benzina
 * 
 * 
 * ESEMPIO
 *   dbpoi.php
 *     POST rect=[{"top":41.931904648177564,"left":12.41709382925842,"bottom":41.85096570364489,"right":12.453344414629214},
 *                {"top":41.95888429635513,"left":12.453344414629214,"bottom":41.85096570364489,"right":12.562096170741597}]
 */

error_reporting(0);
if (isset($_GET['debug'])) $_POST['rect'] = '[{"top":60,"left":5,"bottom":10,"right":20}]';
if (!isset($_POST['rect'])) exit();
$rect = json_decode($_POST['rect']);
if ($rect === NULL || !is_array($rect)) exit();

@include_once 'connection.inc.php';
if (!isset($db_name)) exit('[]');
$db = new mysqli($db_server, $db_user, $db_password, $db_name);
if (mysqli_connect_error()) exit('[]');

$res = array();
$sqlbase = "SELECT CONCAT('db',idarea) id, lat, lng AS lon, nome, prezzibenzina FROM aree_servizio WHERE ";
foreach($rect as $r) {
	$top = $db->real_escape_string($r->top);
	$bottom = $db->real_escape_string($r->bottom);
	$left = $db->real_escape_string($r->left);
	$right = $db->real_escape_string($r->right);
	$sql = "$sqlbase lat BETWEEN '$bottom' AND '$top' AND lng BETWEEN '$left' AND '$right';";
	if (isset($_GET['debug'])) echo "$sql<br>";
	$rs = $db->query($sql);
	if ($rs !== NULL) {
		while($r = $rs->fetch_assoc()) {
			$res[] = $r;
		}
	} 
}

echo json_encode($res);
